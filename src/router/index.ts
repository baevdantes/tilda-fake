import Vue from "vue";
import VueRouter, {RouteConfig} from "vue-router";

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    redirect: {
      path: "/pages"
    }
  },
  {
    path: "/pages",
    component: () => import("@/views/pages.vue")
  },
  {
    path: "/create-page",
    component: () => import("@/views/create-page.vue")
  },
  {
    path: "/page/:id",
    component: () => import("@/views/edit-page.vue")
  },
  {
    path: "*",
    component: () => import("@/views/not-found.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
