import {IPage} from "@/interfaces/IPage";
import {IBlock} from "@/interfaces/IBlock";

export interface IAppState {
  pages: IPage[];
  blocks: IBlock[];
  currentPage: IPage;
}
