import {IBlock} from "@/interfaces/IBlock";

export interface IPage {
  blocks: IBlock[];
  id: number;
}
