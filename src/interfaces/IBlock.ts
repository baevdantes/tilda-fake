export interface IBlock {
  order?: number;
  text: string;
  image?: string;
  type: EnumBlock;
  id?: number;
  title: string;
}

export enum EnumBlock {
  "TEXT" = "textBlock",
  "TEXT_IMAGE" = "textImageBlock",
}

export const BlockNamesMap = {
  [EnumBlock.TEXT_IMAGE]: "Текст + Изображение",
  [EnumBlock.TEXT]: "Текст",
};
