import {MutationTree} from "vuex";
import {IAppState} from "@/interfaces/IAppState";
import {IPage} from "@/interfaces/IPage";

export const mutations: MutationTree<IAppState> = {
  addNewPage: (state, payload: IPage) => {
    state.pages.push(payload);
  },

  setCurrentPAge: (state, payload: IPage) => {
    state.currentPage = payload;
  },

  updatePage: (state, payload: IPage) => {
    state.pages.find((page) => page.id === payload.id).blocks = payload.blocks;
  },

  deletePage: (state, payload: IPage) => {
    const foundPage = state.pages.find((page) => page.id === payload.id);
    if (foundPage) {
      state.pages.splice(state.pages.indexOf(foundPage), 1);
    }
    state.pages.filter((page) => page.id > payload.id).forEach((page) => {
      page.id = page.id - 1;
    });
  }
};
