import {IAppState} from "@/interfaces/IAppState";
import {BlockNamesMap, EnumBlock} from "@/interfaces/IBlock";

export const state: IAppState = {
  pages: [],
  currentPage: null,
  blocks: [
    {
      type: EnumBlock.TEXT,
      text: "",
      title: BlockNamesMap[EnumBlock.TEXT],
    },
    {
      type: EnumBlock.TEXT_IMAGE,
      text: "",
      image: "",
      title: BlockNamesMap[EnumBlock.TEXT_IMAGE],
    }
  ],
};
