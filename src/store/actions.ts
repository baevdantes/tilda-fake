import {ActionTree} from "vuex";
import {IAppState} from "@/interfaces/IAppState";
import {IPage} from "@/interfaces/IPage";

export const actions: ActionTree<IAppState, IAppState> = {
  createPage: ({commit, state}, payload: Omit<IPage, "id">) => {
    const page: IPage = {
      ...payload,
      id: state.pages.length + 1,
    };
    commit("addNewPage", page);
    return page;
  },

  getPage: ({commit, state}, id: number): IPage => {
    const foundPage = state.pages.find((page) => page.id === id);
    if (foundPage) {
      commit("setCurrentPAge", foundPage);
    }
    return foundPage;
  },

  updatePage: ({commit}, payload: IPage) => {
    commit("updatePage", payload);
  },

  deletePage: ({commit}, payload: IPage) => {
    commit("deletePage", payload);
  }
};
