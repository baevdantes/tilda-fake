import Vue from "vue";
import App from "./app.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import "bootstrap-4-grid/css/grid.min.css";
import TextareaAutosize from "vue-textarea-autosize";

Vue.use(TextareaAutosize);

Vue.filter("textareaReplace", (value: string) => value.replace(/\n\r?/g, "<br />"));

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
